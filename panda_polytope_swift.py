import roboticstoolbox as rp
import numpy as np

panda = rp.models.DH.Panda()
# initial pose
q= np.array([0.00138894 ,5.98736e-05,-0.30259058,   -1.6, -6.64181e-05,    1.56995,-5.1812e-05])
panda.q = q
# joint torque limits
t_max = np.array([87, 87, 87, 87, 20, 20, 20]) 
t_min = -t_max

# polytope python module
import pycapacity.robot as capacity_solver

# robot matrices
Jac = panda.jacob0(q)[:3,:]
tau = panda.rne(q, np.zeros((panda.n,)), np.zeros((panda.n,)))
gravity = panda.gravload(q).reshape((-1,1))
print(np.round(Jac,3),panda.qr,gravity,tau)
# calculate for ce polytope
vertices, faces_index =  capacity_solver.force_polytope_withfaces(Jac, t_max, t_min, gravity)
faces = capacity_solver.face_index_to_vertex(vertices,faces_index)
# end effector position
panda_ee_position = panda.fkine(q).t

# visualise panda
panda = rp.models.Panda()
import swift.Swift as Swift
panda.q = q
env = Swift()
env.launch()
env.add(panda)

# polytope visualisaation
import trimesh
# save polytope as mesh file
scaling = 500
mesh = trimesh.Trimesh(vertices=(vertices.T/scaling + panda_ee_position) ,
                       faces=faces_index, process=False, validate=True)
f = open("demofile2.stl", "wb")
f.write(trimesh.exchange.stl.export_stl(mesh))
f.close()
# robot visualisaiton
from spatialgeometry import Mesh
poly_mesh = Mesh('demofile2.stl')
env.add(poly_mesh)


# plotting the polytope using pycapacity
import matplotlib.pyplot as plt
from pycapacity.visual import plot_polytope_faces, plot_polytope_vertex # pycapacity visualisation tools
fig = plt.figure()
# draw faces and vertices
ax = plot_polytope_vertex(plt=plt, vertex=vertices, label='force polytope',color='blue')
plot_polytope_faces(ax=ax, faces=faces, face_color='blue', edge_color='blue', alpha=0.2)

plt.tight_layout()
plt.legend()
plt.show()


