# Robotics toolbox  examples
![](https://github.com/petercorke/robotics-toolbox-python/raw/master/docs/figs/RobToolBox_RoundLogoB.png)
## Download the examples

### By using the terminal:

```bash
git clone https://gitlab.inria.fr/auctus-team/people/antunskuric/example/robotics_toolbox_examples.git
```
### By zip download
Download this repository amd unzip it from the link 
https://gitlab.inria.fr/auctus-team/people/antunskuric/example/robotics_toolbox_examples.git

## Installing the environment
The simplest way to install these python libraries is using anaconda.
```bash
conda env create -f env.yaml    # create the new environemnt and install robotics toolbosx, pycapacity, ...
conda actiavte rtbx_examples
```

### creating a custom envirnoment
You can also simply use anaconda to create a new custom environment:
```bash
conda create -n rtbx_examples python=3.8 pip # create python 3.8 based environment
conda activate rtbx_examples
conda install -c conda-forge roboticstoolbox-python numpy matplotlib 
```

Then install `pycapacity` for the workspace analysis
```bash
pip install pycapacity
```

## Running the examples
Pyplot visualiser test:
```
python panda_animate_pyplot.py
```

Swift visualiser test:
```
python panda_animate_swift.py
```