import roboticstoolbox as rp
import numpy as np

panda = rp.models.DH.Panda()
# initial pose
q= np.array([0.00138894 ,5.98736e-05,-0.30259058,   -1.6, -6.64181e-05,    1.56995,-5.1812e-05])
panda.q = q
# joint torque limits
t_max = np.array([87, 87, 87, 87, 20, 20, 20]) 
t_min = -t_max

# polytope python module
import pycapacity.robot as capacity_solver

# robot matrices
Jac = panda.jacob0(q)[:3,:]
tau = panda.rne(q, np.zeros((panda.n,)), np.zeros((panda.n,)))
gravity = panda.gravload(q).reshape((-1,1))
print(np.round(Jac,3),panda.qr,gravity,tau)
# calculate for ce polytope
vertices, faces_index =  capacity_solver.force_polytope_withfaces(Jac, t_max, t_min, gravity)
# end effector position
panda_ee_position = panda.fkine(q).t
# scale the polytiope and place it to the end-effector
scaling = 500
faces = capacity_solver.face_index_to_vertex(vertices/scaling + panda_ee_position[:, None],faces_index)


# plotting the polytope using pycapacity
import matplotlib.pyplot as plt
from pycapacity.visual import plot_polytope_faces, plot_polytope_vertex # pycapacity visualisation tools

# visualise panda
fig = panda.plot(q)
ax = fig.ax

# draw faces and vertices
plot_polytope_vertex(ax=ax, vertex=vertices, label='force polytope',color='blue')
plot_polytope_faces(ax=ax, faces=faces, face_color='blue', edge_color='blue', alpha=0.2)

ax.set_xlim([-1, 1.5])
ax.set_ylim([-1, 1.5])
ax.set_zlim([0, 1.5])
plt.tight_layout()
plt.legend()
plt.show()
fig.hold()

