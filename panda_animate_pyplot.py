import roboticstoolbox as rtb

robot = rtb.models.DH.Panda()  # load URDF version of the Panda
env = robot.plot(robot.qz)

from spatialmath import SE3
# swet target location
T = SE3(0.7, 0.2, 0.1) * SE3.OA([0, 1, 0], [0, 0, -1])
sol = robot.ikine_LM(T)         # solve IK
print(sol.q)
q_pickup = sol.q

qt = rtb.jtraj(robot.qz, q_pickup, 50)
for qk in qt.q:             # for each joint configuration on trajectory
      robot.q = qk          # update the robot state
      env.step()        # update visualization
env.hold()
