from time import time
import roboticstoolbox as rtb
from swift.Swift import Swift  # instantiate 3D browser-based visualizer\

robot = rtb.models.URDF.Panda()  # load URDF version of the Panda
env = Swift()
env.launch(realtime=True)            # activate it
env.add(robot)          # add robot to the 3D scene

from spatialmath import SE3
# swet target location
T = SE3(0.7, 0.2, 0.1) * SE3.OA([0, 1, 0], [0, 0, -1])
sol = robot.ikine_LM(T)         # solve IK
print(sol.q)
q_pickup = sol.q

qt = rtb.jtraj(robot.qz, q_pickup, 50)
for qk in qt.q:             # for each joint configuration on trajectory
      robot.q = qk          # update the robot state
      env.step()        # update visualization

env.hold()
